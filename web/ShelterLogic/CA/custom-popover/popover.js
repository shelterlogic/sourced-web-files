jQuery(document).ready(function() {
	/*
	|--------------------------------------------------------------------------
	| Document ready state
	|--------------------------------------------------------------------------
	*/
	jQuery('a.poplink').click(function(e) {
		e.preventDefault();
		var $this = jQuery(this);
		var this_href = $this.attr('href');
		jQuery(this_href).fadeIn('fast');
		window.scrollTo(0,0);
	});
	jQuery('.popclose').click(function(e) {
		e.preventDefault();
		
		jQuery('.quote').fadeOut('fast');
		window.scrollTo(0,1030);
	});
	
});