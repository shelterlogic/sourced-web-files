# Sourced Web Files

In this repo we keep all the files we would like to soure to diferent web assets to maintain a low text to code ratio as will as a central point to manage assets.

1. Branch "develop" locally and follow gitflow for rebase, merge process to develop
2. Create a folder and potentially sub-folders for you assets i.e. "ShelterTech" -> "SPSeries", should be project based and something that may be used in multiple locations.
3. Create an asset i.e. thickbox.style.css or thickbox.style.scss and compile to thickbox.style.css (Note if creating compilable assets you should create a sub-folder for collection and sub-folders for breakouts)
4. Create README.md and identify all locations that are sourcing files, make sure your assets are commented. If no README file is found your future pull requests will be denied.
5. Implement, test and confirm functionality. (Keep in mind if you delete a file you there may be web assets linking to it so read the README in each folder to identify breakpoints if files are deleted)
6. Rebase from remote develop (resolve conflicts)
7. Checkout develop, merge from your local branch
8. Push to remote develop
9. Test again
10. Rebase develop from remote master (resolve conflicts)
11. Checkout master, merge from remote develop
12. Push master to remote
13. Tag with note of your changes