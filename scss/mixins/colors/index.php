<?php include('../../../includes/functions.php');
	   
    // get the universal header
    get_template_part('header'); 

    ?>
		<div class="brand-colors shelter-yellow"><span>$shelter-yellow: #F0B310;</span></div>
		<div class="brand-colors shelter-grey"><span>$shelter-grey: #333333;</span></div>
		<div class="brand-colors shelter-blue"><span>$shelter-blue: #33648A;</span></div>
		<div class="brand-colors shelter-red"><span>$shelter-red: #ff0000;</span></div>
		<div class="brand-colors shelter-burnt"><span>$shelter-burnt: #7B2101;</span></div>
		<div class="brand-colors dark-grey"><span>$dark-grey: #555555;</span></div>
		<div class="brand-colors light-grey"><span>$light-grey: #cccccc;</span></div>
		<div class="brand-colors grey"><span>$grey: #777777;</span></div>
		<div class="brand-colors green"><span>$green: #309369;</span></div>
		<link rel="stylesheet" href="./brand.colors.example.css">
	<?php 
		// get the footer
	    get_template_part('footer');
	    
	    // get script inclides for home
	    get_template_part('default-scripts'); ?>
		
		<script type="text/javascript">
			$(document).ready(function() {
				/*
				|--------------------------------------------------------------------------
				| Document ready state
				|--------------------------------------------------------------------------
				*/
				$('div.brand-colors').makeSQ();
				$('div.brand-colors span').centerCenter();

			});
			$(window).resize(function(){
				/*
				|--------------------------------------------------------------------------
				| window resize
				|--------------------------------------------------------------------------
				*/
				$('div.brand-colors').makeSQ();
				$('div.brand-colors span').centerCenter();
			});	
		</script>

    <!-- close the doc -->
<?php get_template_part('close'); ?>