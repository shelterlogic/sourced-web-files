// assets list
ko.applyBindings({
    asset: [
        { 
            title: 'ShelterTech SCSS || CSS', 
            thumb: 'http://placehold.it/150x200&text=image', 
            author: 'Bryan Bielefeldt',
            pubdate: 'June 2014', 
            description: 'This file is being used on SL.com', 
            type: 'SCSS|CSS', 
            source: 'scss/SL-com/ShelterTech/sheltertech.landing.css', 
            links: [ 
                [
                    'Used on ST Landing Category Page',
                    'http://www.shelterlogic.com/CategoryDetail.aspx?CategoryName=ProGrade%20Shelters'
                ], [
                    'Used on ST test page',
                    'http://www.shelterlogic.com/Content.aspx?PageName=_SP%20Form%20Test#configure'
                ], [
                    'Add your include here',
                    'and/your/link'
                ] ]
        },
        { 
        	title: 'All Mixins SCSS || CSS', 
        	thumb: 'http://placehold.it/150x200&text=image', 
        	author: 'Bryan Bielefeldt', 
        	pubdate: 'June 2014', 
        	description: 'This file includes all of the below styles and variables and can be used as a package. The assets can however be imported individually to you scss as well. Location: scss>mixins>all.mixins.scss||css',
            type: 'SCSS|CSS',  
        	source: 'scss/mixins/all.mixins.css', 
        	links: [ 
        		[
        			'scss imported to sheltertech.landing.scss',
        			'scss/SL-com/ShelterTech/sheltertech.landing.css'
        		], [
        			'Add your include here',
        			'and/your/link'
        		] ]
        },
        { 
        	title: 'Brand Colors SCSS', 
        	thumb: 'http://placehold.it/150x200&text=image', 
        	author: 'Bryan Bielefeldt', 
        	pubdate: 'June 2014', 
        	description: 'This SCSS file contains all the brand colors and can be importedt to your SCSS', 
        	source: '/scss/mixins/colors/brand.colors.scss', 
            type: 'SCSS', 
        	links: [ 
        		[
        			'Imported into all.mixins.scss',
        			'/scss/mixins/all.mixins.scss'
        		], [
                    'Add your include here',
                    'and/your/link'
                ] ]
        },
        { 
        	title: 'Bug yellow Buttons Mixin SCSS', 
        	thumb: 'http://placehold.it/150x200&text=image', 
        	author: 'Bryan Bielefeldt', 
        	pubdate: 'June 2014', 
        	description: 'Styling and hover for Big yellow buttons', 
            type: 'SCSS', 
        	source: '/scss/mixins/buttons/big.yellow.scss', 
        	links: [ 
        		[
                    'Imported into all.mixins.scss',
                    '/scss/mixins/all.mixins.scss'
                ], [
                    'Add your include here',
                    'and/your/link'
                ] ]
        },
        { 
            title: 'Transisions Mixin SCSS', 
            thumb: 'http://placehold.it/150x200&text=image', 
            author: 'Bryan Bielefeldt', 
            pubdate: 'June 2014', 
            description: 'This is a traditional SCSS @mixin requiring 2 parameters type (i.e. background-color, all) and speed (i.e. 500ms)', 
            type: 'SCSS', 
            source: '/scss/mixins/transitions/transitions.scss', 
            links: [ 
                [
                    'Imported into all.mixins.scss',
                    '/scss/mixins/all.mixins.scss'
                ], [
                    'Add your include here',
                    'and/your/link'
                ] ]
        },
        { 
            title: 'Opacity Mixin SCSS', 
            thumb: 'http://placehold.it/150x200&text=image', 
            author: 'Bryan Bielefeldt', 
            pubdate: 'June 2014', 
            description: 'This is a traditional SCSS @mixin requiring 1 parameter opacity (i.e. 0.9 for 90% or 1 for 100%)',
            type: 'SCSS', 
            source: '/scss/mixins/opacity/opacity.scss', 
            links: [ 
                [
                    'Imported into all.mixins.scss',
                    '/scss/mixins/all.mixins.scss'
                ], [
                    'Add your include here',
                    'and/your/link'
                ] ]
        }
        
    ]
});